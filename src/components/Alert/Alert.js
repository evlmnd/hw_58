import React from 'react';

import './Alert.css';

const Alert = props => {
    return (
        <div
            className={['Alert', props.type].join(' ')}
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
        >
            {props.children}
            <button
                className="Alert-Button"
                onClick={props.dismiss}
                style={{
                    transform: props.dismiss ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.dismiss ? '1' : '0'
                }}
            >X</button>
        </div>
    );
};

export default Alert;