import React, {Fragment} from 'react';
import Backdrop from '../Backrdop/Backdrop';

import './Modal.css';

const Modal = props => {
    return (
        <Fragment>
            <Backdrop show={props.show} onClick={props.close}/>
            <div className="Modal"
                 style={{
                     transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                     opacity: props.show ? '1' : '0'
                 }}
            >
                <p className="modal-title">{props.title}</p>
                <button className="close-button" onClick={props.close}>x</button>
                {props.children}
            </div>
        </Fragment>
        
    )
};

export default Modal;