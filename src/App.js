import React, {Component} from 'react';
import Modal from './components/Modal/Modal';
import Alert from './components/Alert/Alert';

import './App.css';

class App extends Component {

    state = {
        showingModal: false,
        showingAlert: false
    };

    closeModalWindow = () => {
        this.setState({showingModal: false});
    };

    showModalWindow = () => {
        this.setState({showingModal: true});
    };

    closeAlert = () => {
        this.setState({showingAlert: false})
    };

    showAlert = () => {
        this.setState({showingAlert: true})
    };

    render() {
        return (
            <div className="App">
                <div className="Buttons">
                    <button className="Button" onClick={this.showModalWindow}>Show Modal!</button>
                    <button className="Button" onClick={this.showAlert}>Show Alerts!</button>

                </div>
                <Modal
                    show={this.state.showingModal}
                    close={this.closeModalWindow}
                    title="This is title"
                >
                    <p>This is message</p>
                </Modal>

                <Alert
                    show={this.state.showingAlert}
                    type="warning"
                    dismiss={this.closeAlert}
                >
                    This is a warning type alert
                </Alert>
                <Alert
                    show={this.state.showingAlert}
                    type="success"
                >
                    This is a success type alert
                </Alert>
                <Alert
                    show={this.state.showingAlert}
                    type="primary"
                    dismiss={this.closeAlert}
                >
                    This is a primary type alert
                </Alert>
                <Alert
                    show={this.state.showingAlert}
                    type="danger"
                >
                    This is a danger type alert
                </Alert>
            </div>
        );
    }
}

export default App;
